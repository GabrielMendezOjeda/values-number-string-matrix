#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

#include "jni_inout.h"
#include "library.c"
extern JNIEnv *javaEnv;

/**
 * @author Francisco Guerra (francisco.guerra@ulpgc.es)
 * @version 1.0
 */

JNIEXPORT jint JNICALL Java_library_valuesNumber_1
  (JNIEnv *env, jobject object, jstring foundValue, jobjectArray value, jint valueRows, jint valueColumns)
{
    javaEnv = env;
    char* c_foundValue = toString(foundValue);
    int c_valueRows = toInt(valueRows);
    int c_valueColumns = toInt(valueColumns);
    char*** c_value = toStringMatrix(value, valueRows, valueColumns);
    int c_outValue = valuesNumber(c_foundValue, c_value, c_valueRows, c_valueColumns);
    return toJint(c_outValue);
}
