#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include "jni_test.h"
#include "library.c"

/**
 * @author Francisco Guerra (francisco.guerra@ulpgc.es)
 * @version 1.0
 */

void testExercise_A() {
	// Given
	const char* lookValue = "Javier";
	const char*** matriz = (const char***)malloc(1 * sizeof(char**));
	////////////row 1 column 1////////////////////
	matriz[0] = (const char**)malloc(1 * sizeof(char*));
	matriz[0][0] = "Eva";
	// When
	int result = valuesNumber(lookValue, matriz, 1, 1);
	// Then
	assertEquals_int(0, result);
	}

void testExercise_B() {
    // Given
    
    // When
    
    // Then
    fail("Not yet implemented");
}

void testExercise_C() {
    // Given
    
    // When
    
    // Then
    fail("Not yet implemented");
}

void testExercise_D() {
    // Given
    
    // When
    
    // Then
    fail("Not yet implemented");
}

void testExercise_E() {
    // Given
    
    // When
    
    // Then
    fail("Not yet implemented");
}
